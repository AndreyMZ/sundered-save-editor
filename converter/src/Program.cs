﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

using Newtonsoft.Json;


namespace SunderedSaveEditor
{
	internal class Program
	{
		static Assembly assemblyCSharp;

		static void Main(string[] args)
		{
			if (!((args.Length == 4 && args[0] == "bin2json") ||
			      (args.Length == 5 && args[0] == "json2bin") ||
			      (args.Length == 5 && args[0] == "test"))) {
				Usage();
				return;
			}
			
			string operation = args[0];
			string assembliesDir = args[1];
			string binSavePath = args[2];
			string jsonSavePath = args[3];

			try {
				LogDebug("Load assembly Assembly-CSharp.dll");
				assemblyCSharp = Assembly.LoadFrom(assembliesDir + "\\Assembly-CSharp.dll");
				LogDebug("Load assembly UnityEngine.dll");
				Assembly.LoadFrom(assembliesDir + "\\UnityEngine.dll");
			} catch (IOException ex) {
				Console.WriteLine(String.Format("Error: {0}", ex.Message));
				Environment.Exit(1);
			}

			LogDebug("LoadBin...");
			object data = LoadBin(binSavePath);
			LogDebug("LoadBin done.");
			if (operation == "bin2json" || operation == "test") {
				SaveSlots save = ExtractFromData(data);
				DumpJson(jsonSavePath, save);
			} 
			if (operation == "json2bin" || operation == "test") {
				string newBinSavePath = args[4];
				SaveSlots save = LoadJson(jsonSavePath);
				UpdateData(data, save);
				SaveBin(newBinSavePath, data);
			}
		}

		static void Usage() {
			Console.WriteLine("Usage:");
			Console.WriteLine("convert.exe bin2json ASSEMBLIES_DIR BIN_SAVE_PATH JSON_SAVE_PATH");
			Console.WriteLine("convert.exe json2bin ASSEMBLIES_DIR BIN_SAVE_PATH JSON_SAVE_PATH NEW_BIN_SAVE_PATH");
		}

		static void LogError(string message) {
			Console.Error.WriteLine(String.Format("[ERROR] {0}", message));
		}

		static void LogDebug(string message) {
			Console.Error.WriteLine(String.Format("[DEBUG] {0}", message));
		}

		static object LoadBin(string path) {
			try {
				BinaryFormatter binaryFormatter = new BinaryFormatter();
				using (FileStream file = File.Open(path, FileMode.Open)) {
					return binaryFormatter.Deserialize(file);
				}
			} catch (IOException ex) {
				Console.WriteLine(String.Format("Error: {0}", ex.Message));
				Environment.Exit(1);
				return null; // Unreachable.
			} catch (Exception ex) {
				LogError(ex.ToString());
				Console.WriteLine(String.Format("Error: {0}: {1}", ex.GetType().FullName, ex.Message));
				Environment.Exit(1);
				return null; // Unreachable.
			}
		}

		static void SaveBin(string path, object data) {
			try {
				BinaryFormatter binaryFormatter = new BinaryFormatter();
				using (FileStream file = File.Create(path)) {
					binaryFormatter.Serialize(file, data);
				}
			} catch (IOException ex) {
				Console.WriteLine(String.Format("Error: {0}", ex.Message));
				Environment.Exit(1);
			}
		}

		static void DumpJson(string path, SaveSlots save) {
			try {
				JsonSerializer serializer = new JsonSerializer();
				serializer.Formatting = Formatting.Indented;
				using (JsonTextWriter writer = new JsonTextWriter(new StreamWriter(path)))
				{
					writer.IndentChar = '\t';
					writer.Indentation = 1;
					serializer.Serialize(writer, save);
				}
			} catch (IOException ex) {
				Console.WriteLine(String.Format("Error: {0}", ex.Message));
				Environment.Exit(1);
			}
		}

		static SaveSlots LoadJson(string path) {
			try {
				JsonSerializer serializer = new JsonSerializer();
				using (JsonTextReader reader = new JsonTextReader(new StreamReader(path))) {
					return serializer.Deserialize<SaveSlots>(reader);
				}
			} catch (IOException ex) {
				Console.WriteLine(String.Format("Error: {0}", ex.Message));
				Environment.Exit(1);
				return null; // Unreachable.
			}
		}

		static SaveSlots ExtractFromData(object data) {
			SaveSlots result = new SaveSlots();
			foreach (var datum in (IList)data) {
				SaveGame save = new SaveGame();
				save.difficulty = GetField(datum, "difficulty").ToString();
				save.gameplayTime = GetField<float>(datum, "gameplayTime");
				save.shards = GetField<int>(datum, "shards");
				save.totalShardsGathered = GetField<int>(datum, "totalShardsGathered");
				save.elderFragments = GetField<int>(datum, "elderFragments");
				save.discoveredRooms = GetField<List<int>>(datum, "discoveredRooms");
				save.playedOnceEverVO = GetField<List<int>>(datum, "playedOnceEverVO");
				
				IList _activePerks = GetField<IList>(GetField(datum, "perkData"), "_activePerks");
				save.perkData._activePerks.Clear();
				foreach (var perk in _activePerks) {
					save.perkData._activePerks.Add(perk.ToString());
				}

				IList _availablePerks = GetField<IList>(GetField(datum, "perkData"), "_availablePerks");
				save.perkData._availablePerks.Clear();
				foreach (var perk in _availablePerks) {
					save.perkData._availablePerks.Add(perk.ToString());
				}

				IList _unseenPerks = GetField<IList>(GetField(datum, "perkData"), "_unseenPerks");
				save.perkData._unseenPerks.Clear();
				foreach (var perk in _unseenPerks) {
					save.perkData._unseenPerks.Add(perk.ToString());
				}

				IDictionary upgradeValues = GetField<IDictionary>(GetField(datum, "upgradeSystem"), "upgradeValues");
				save.upgradeSystem.upgradeValues.Clear();
				foreach (DictionaryEntry item in upgradeValues) {
					save.upgradeSystem.upgradeValues.Add(
						item.Key.ToString(),
						(Dictionary<string, int>)item.Value
					);
				}

				IList flags = GetField<IList>(datum, "flags");
				save.flags.Clear();
				foreach (var flag in flags) {
					save.flags.Add(flag.ToString());
				}
				
				result.Add(save);
			}
			return result;
		}

		static void UpdateData(object data, SaveSlots saves) {
			Type Difficulty = assemblyCSharp.GetType("Difficulty", true);
			Type PerkType = assemblyCSharp.GetType("PerkType", true);
			Type GameFlag = assemblyCSharp.GetType("GameFlag", true);
			Type UpgradeData = assemblyCSharp.GetType("UpgradeData", true);
			Type UpgradeType = UpgradeData.GetNestedType("UpgradeType", BindingFlags.Public | BindingFlags.NonPublic);

			for (int i=0; i<saves.Count; i++) {
				SaveGame save = saves[i];
				object datum = ((IList)data)[i];

				SetField(datum, "difficulty", Enum.Parse(Difficulty, save.difficulty));
				SetField(datum, "gameplayTime", save.gameplayTime);
				SetField(datum, "shards", save.shards);
				SetField(datum, "totalShardsGathered", save.totalShardsGathered);
				SetField(datum, "elderFragments", save.elderFragments);
				SetField(datum, "discoveredRooms", save.discoveredRooms);
				SetField(datum, "playedOnceEverVO", save.playedOnceEverVO);

				IList _activePerks = GetField<IList>(GetField(datum, "perkData"), "_activePerks");
				_activePerks.Clear();
				foreach (var perk in save.perkData._activePerks) {
					_activePerks.Add(Enum.Parse(PerkType, perk));
				}

				IList _availablePerks = GetField<IList>(GetField(datum, "perkData"), "_availablePerks");
				_availablePerks.Clear();
				foreach (var perk in save.perkData._availablePerks) {
					_availablePerks.Add(Enum.Parse(PerkType, perk));
				}

				IList _unseenPerks = GetField<IList>(GetField(datum, "perkData"), "_unseenPerks");
				_unseenPerks.Clear();
				foreach (var perk in save.perkData._unseenPerks) {
					_unseenPerks.Add(Enum.Parse(PerkType, perk));
				}

				/* 
				 * Setting any value in `upgradeSystem.upgradeValues` to a new `Dictionary` breaks the save.
				 * Sundered cannot load it. I don't know why.
				 * So, we cannot add new items. We will only remove unneeded items and change existing ones.
				 */
				IDictionary upgradeValues = GetField<IDictionary>(GetField(datum, "upgradeSystem"), "upgradeValues");
				List<object> keysCopy = upgradeValues.Keys.Cast<object>().ToList();
				foreach (object key in keysCopy) {
					if (!save.upgradeSystem.upgradeValues.ContainsKey(key.ToString())) {
						upgradeValues.Remove(key);
					}
				}
				foreach (var item in save.upgradeSystem.upgradeValues) {
					object key = Enum.Parse(UpgradeType, item.Key);
					if (upgradeValues.Contains(key)) {
						Dictionary<string, int> oldUpgrades = (Dictionary<string, int>)upgradeValues[key];
						Dictionary<string, int> newUpgrades = item.Value;
						oldUpgrades.Clear();
						foreach (var upgrade in newUpgrades) {
							oldUpgrades.Add(upgrade.Key, upgrade.Value);
						}
					}
					else {
						// upgradeValues.Add(key, item.Value); // Does not work =(.
						Console.WriteLine(String.Format("Error: Upgrade value \"{0}\" does not exist. Adding upgrade values is not supported.", key));
						Environment.Exit(1);
					}
				}

				IList flags = GetField<IList>(datum, "flags");
				flags.Clear();
				foreach (var flag in save.flags) {
					flags.Add(Enum.Parse(GameFlag, flag));
				}
			}
		}

		
		static T GetField<T>(object obj, string name) {
			return (T)obj.GetType().GetField(name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(obj);
		}
		static object GetField(object obj, string name) {
			return GetField<object>(obj, name);
		}

		static void SetField(object obj, string name, object value) {
			obj.GetType().GetField(name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).SetValue(obj, value);
		}
	}
}
