using System.Collections.Generic;
using System.Xml.Serialization;


public class SaveSlots: List<SaveGame>
{
}

public class SaveGame
{
	public string difficulty;
	public float gameplayTime;
	public int shards;
	public int totalShardsGathered;
	public int elderFragments;
	public PerkData perkData = new PerkData();
	public UpgradeData upgradeSystem = new UpgradeData();
	public List<int> discoveredRooms = new List<int>();
	public List<int> playedOnceEverVO = new List<int>();
	public List<string> flags = new List<string>();
}

public class PerkData
{
	public List<string> _activePerks = new List<string>();
	public List<string> _availablePerks = new List<string>();
	public List<string> _unseenPerks = new List<string>();
}

public class UpgradeData
{
	public Dictionary<string, Dictionary<string, int>> upgradeValues = new Dictionary<string, Dictionary<string, int>>();
}
