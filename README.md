Requirements
============

- [Python][] 3.12
- Sundered: Eldritch Edition (on [Steam][] or [Epic Games Store][])
- [Mono for Windows][], latest version (tested with 6.12.0.206), either 64-bit or 32-bit - for the Steam version of the game\
  or [Mono 2.11.4 for Windows][] - for the EGS version of the game


Installation
============

1. Install [pipx][].
2. Run:
    
        pipx install sundered-save-editor --index-url https://gitlab.com/api/v4/projects/AndreyMZ%2Fsundered-save-editor/packages/pypi/simple


Usage
=====

Run:

      sundered-save-editor


[Python]: https://www.python.org/
[Mono for Windows]: https://www.mono-project.com/download/stable/#download-win
[Mono 2.11.4 for Windows]: https://download.mono-project.com/archive/2.11.4/windows-installer/
[Steam]: https://store.steampowered.com/app/535480/Sundered_Eldritch_Edition/
[Epic Games Store]: https://store.epicgames.com/ru/p/sundered-eldritch-edition
[pipx]: https://pypa.github.io/pipx/#install-pipx


Links
=====

- [NEED HELP WITH MY SAVE!](https://thunderlotusgames.com/support/sundered/technical/help-save/)
- [Modifying save files](https://steamcommunity.com/app/535480/discussions/0/1696045708653951098/)
- [How to edit a save game to un-corrupt an ability (I didnt mean to corrupt it)](https://steamcommunity.com/app/535480/discussions/0/3247562523084541073/)
- [Accidentally corrupted an ability. Need to edit save file](https://www.reddit.com/r/sundered/comments/aryb00/accidentally_corrupted_an_ability_need_to_edit/)
- <https://www.saveeditonline.com>
