1.0.0 (2024-11-10)

- Improve support for the Steam version of the game.

0.1.0 (2022-11-06)

- Initial release.
