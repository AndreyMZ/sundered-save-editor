from __future__ import annotations

import json
import time
from copy import deepcopy
from functools import partial
from pathlib import Path
from subprocess import CalledProcessError
from tempfile import TemporaryDirectory
from typing import Optional, cast

from PySide6.QtCore import QCoreApplication, Slot
from PySide6.QtWidgets import QComboBox, QFileDialog, QMainWindow

from .model.save import SaveGame, SaveSlots
from .tab import Tab
from .ui.main_window import Ui_MainWindow
from .utils import execlib
from .utils.execlib import To
from .utils.qt import ShowErrorMessageBox

CONVERTER_PATH = Path(__file__).parent.joinpath("bin", "convert.exe")


def tr(text: str) -> str:
	# noinspection PyTypeChecker
	return QCoreApplication.translate("MainWindow", text)


class MainWindow(QMainWindow):
	def __init__(self) -> None:
		super().__init__()
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)

		self.ui.action_import_json.triggered.connect(self.action_import_json)
		self.ui.action_export_json.triggered.connect(self.action_export_json)
		self.ui.actionExit.triggered.connect(exit)
		self.ui.action_slot1to2.triggered.connect(partial(self.action_copy_slot, 0, 1))
		self.ui.action_slot1to3.triggered.connect(partial(self.action_copy_slot, 0, 2))
		self.ui.action_slot2to1.triggered.connect(partial(self.action_copy_slot, 1, 0))
		self.ui.action_slot2to3.triggered.connect(partial(self.action_copy_slot, 1, 2))
		self.ui.action_slot3to1.triggered.connect(partial(self.action_copy_slot, 2, 0))
		self.ui.action_slot3to2.triggered.connect(partial(self.action_copy_slot, 2, 1))

		self.ui.edit_savefile.setText(str(Path.home().joinpath("AppData/LocalLow/Thunder Lotus Games/Sundered/SAVE_GAME.sav")))
		_filter_existing_paths(self.ui.edit_mono)
		_filter_existing_paths(self.ui.edit_gamedir)

		self.ui.btn_mono.clicked.connect(self.btn_mono_clicked)
		self.ui.btn_gamedir.clicked.connect(self.btn_gamedir_clicked)
		self.ui.btn_savefile.clicked.connect(self.btn_savefile_clicked)
		self.ui.btn_load.clicked.connect(self.btn_load_clicked)
		self.ui.btn_save.clicked.connect(self.btn_save_clicked)

		self.ui.tabs.addTab(Tab(), tr("Slot 1"))
		self.ui.tabs.addTab(Tab(), tr("Slot 2"))
		self.ui.tabs.addTab(Tab(), tr("Slot 3"))

		self.ui.action_export_json.setEnabled(False)
		self.ui.menuEdit.setEnabled(False)
		self.ui.tabs.setEnabled(False)
		self.ui.btn_save.setEnabled(False)

		self.slots: Optional[SaveSlots] = None
		self.temp_dir = TemporaryDirectory()
		self.json_save_path = Path(self.temp_dir.name, "SAVE_GAME.json")

	@Slot()
	def cleanup(self) -> None:
		self.temp_dir.cleanup()

	@Slot()
	def btn_mono_clicked(self) -> None:
		filepath, _ = QFileDialog.getOpenFileName(
			parent=self,
			caption=tr("Select Mono binary"),
			dir=self.ui.edit_mono.currentText(),
			filter=";;".join([tr("Mono binary (mono.exe)"), tr("All files (*)")]),
		)
		if filepath:
			self.ui.edit_mono.setCurrentText(filepath)

	@Slot()
	def btn_gamedir_clicked(self) -> None:
		dirpath = QFileDialog.getExistingDirectory(
			parent=self,
			caption=tr("Select game directory"),
			dir=self.ui.edit_gamedir.currentText(),
		)
		if dirpath:
			self.ui.edit_gamedir.setCurrentText(dirpath)

	@Slot()
	def btn_savefile_clicked(self) -> None:
		filepath, _ = QFileDialog.getOpenFileName(
			parent=self,
			caption=tr("Select save file"),
			dir=self.ui.edit_savefile.text(),
			filter=";;".join([tr("Save files (*.sav)"), tr("All files (*)")]),
		)
		if filepath:
			self.ui.edit_savefile.setText(filepath)

	@Slot()
	def btn_load_clicked(self) -> None:
		cmd: list[str|Path] = [
			Path(self.ui.edit_mono.currentText()),
			"--runtime=v2.0.50727",
			CONVERTER_PATH,
			"bin2json",
			Path(self.ui.edit_gamedir.currentText(), "Sundered_Data", "Managed"),
			Path(self.ui.edit_savefile.text()),
			self.json_save_path,
		]
		try:
			execlib.run(cmd, stdout=To.PIPE).check_returncode()
		except FileNotFoundError as ex:
			ShowErrorMessageBox("Cannot run the Mono executable.", str(ex))
		except CalledProcessError as ex:
			ShowErrorMessageBox("Cannot load the save file.", ex.stdout.decode('UTF-8'))
		else:
			self.load(self.json_save_path)
			self.statusBar().showMessage(f"Loaded. Intermediate JSON: {self.json_save_path}")

	@Slot()
	def btn_save_clicked(self) -> None:
		self.save(self.json_save_path)
		old_save_path = Path(self.ui.edit_savefile.text())
		new_save_path = Path(self.temp_dir.name, "SAVE_GAME.sav")
		bak_save_path = Path(self.temp_dir.name, f"SAVE_GAME_{time.monotonic_ns()}.sav.bak")
		cmd: list[str|Path] = [
			Path(self.ui.edit_mono.currentText()),
			"--runtime=v2.0.50727",
			CONVERTER_PATH,
			"json2bin",
			Path(self.ui.edit_gamedir.currentText(), "Sundered_Data", "Managed"),
			old_save_path,
			self.json_save_path,
			new_save_path,
		]
		try:
			execlib.run(cmd, stdout=To.PIPE).check_returncode()
		except CalledProcessError as ex:
			ShowErrorMessageBox("Cannot load the save file.", ex.stdout.decode('UTF-8'))
		else:
			old_save_path.rename(bak_save_path)
			new_save_path.rename(old_save_path)
			self.statusBar().showMessage(f"Saved. Backup: {bak_save_path}")

	def load(self, json_save_path: Path) -> None:
		with json_save_path.open('rt') as file:
			self.slots = cast(SaveSlots, json.load(file))
		assert len(self.slots) == 3
		for i in range(3):
			tab = cast(Tab, self.ui.tabs.widget(i))
			tab.load(self.slots[i])

		self.ui.action_export_json.setEnabled(True)
		self.ui.menuEdit.setEnabled(True)
		self.ui.tabs.setEnabled(True)
		self.ui.btn_save.setEnabled(True)

	def save(self, json_save_path: Path) -> None:
		assert self.slots is not None
		for i in range(3):
			tab = cast(Tab, self.ui.tabs.widget(i))
			self.slots[i] = tab.save()
		with json_save_path.open('wt') as file:
			json.dump(self.slots, file)

	@Slot()
	def action_copy_slot(self, n_from: int, n_to: int) -> None:
		assert self.slots is not None
		copy = {k: deepcopy(v) for k, v in self.slots[n_from].items() if k != 'upgradeSystem'}
		upgrades_from = self.slots[n_from]['upgradeSystem']['upgradeValues']
		upgrades_to = self.slots[n_to]['upgradeSystem']['upgradeValues']
		copy['upgradeSystem'] = {
			'upgradeValues': {k: deepcopy(v) for k,v in upgrades_from.items() if k in upgrades_to}
		}

		self.slots[n_to] = cast(SaveGame, copy)
		tab_to = cast(Tab, self.ui.tabs.widget(n_to))
		tab_to.load(self.slots[n_to])

	@Slot()
	def action_import_json(self) -> None:
		filepath, _ = QFileDialog.getOpenFileName(
			parent=self,
			filter=";;".join([tr("JSON files (*.json)"), tr("All files (*)")]),
		)
		if filepath:
			self.load(Path(filepath))
			self.statusBar().showMessage(f"Imported JSON from {filepath}")

	@Slot()
	def action_export_json(self) -> None:
		filepath, _ = QFileDialog.getSaveFileName(
			parent=self,
			filter=";;".join([tr("JSON files (*.json)"), tr("All files (*)")]),
		)
		if filepath:
			self.save(Path(filepath))
			self.statusBar().showMessage(f"Exported JSON to {filepath}")


def _filter_existing_paths(combobox: QComboBox) -> None:
	for i in reversed(range(combobox.count())):
		if not Path(combobox.itemText(i)).exists():
			combobox.removeItem(i)
