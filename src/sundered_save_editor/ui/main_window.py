# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.7.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QFormLayout, QHBoxLayout,
    QLabel, QLineEdit, QMainWindow, QMenu,
    QMenuBar, QPushButton, QSizePolicy, QStatusBar,
    QTabWidget, QVBoxLayout, QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(750, 550)
        self.actionExit = QAction(MainWindow)
        self.actionExit.setObjectName(u"actionExit")
        self.action_slot1to2 = QAction(MainWindow)
        self.action_slot1to2.setObjectName(u"action_slot1to2")
        self.action_slot1to3 = QAction(MainWindow)
        self.action_slot1to3.setObjectName(u"action_slot1to3")
        self.action_slot2to1 = QAction(MainWindow)
        self.action_slot2to1.setObjectName(u"action_slot2to1")
        self.action_slot2to3 = QAction(MainWindow)
        self.action_slot2to3.setObjectName(u"action_slot2to3")
        self.action_slot3to2 = QAction(MainWindow)
        self.action_slot3to2.setObjectName(u"action_slot3to2")
        self.action_slot3to1 = QAction(MainWindow)
        self.action_slot3to1.setObjectName(u"action_slot3to1")
        self.action_import_json = QAction(MainWindow)
        self.action_import_json.setObjectName(u"action_import_json")
        self.action_export_json = QAction(MainWindow)
        self.action_export_json.setObjectName(u"action_export_json")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout_1 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_1.setObjectName(u"verticalLayout_1")
        self.horizontalLayout_1 = QHBoxLayout()
        self.horizontalLayout_1.setObjectName(u"horizontalLayout_1")
        self.formLayout_1 = QFormLayout()
        self.formLayout_1.setObjectName(u"formLayout_1")
        self.label_1 = QLabel(self.centralwidget)
        self.label_1.setObjectName(u"label_1")

        self.formLayout_1.setWidget(0, QFormLayout.LabelRole, self.label_1)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.edit_mono = QComboBox(self.centralwidget)
        self.edit_mono.addItem("")
        self.edit_mono.addItem("")
        self.edit_mono.addItem("")
        self.edit_mono.setObjectName(u"edit_mono")
        sizePolicy = QSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.edit_mono.sizePolicy().hasHeightForWidth())
        self.edit_mono.setSizePolicy(sizePolicy)
        self.edit_mono.setEditable(True)

        self.horizontalLayout_2.addWidget(self.edit_mono)

        self.btn_mono = QPushButton(self.centralwidget)
        self.btn_mono.setObjectName(u"btn_mono")

        self.horizontalLayout_2.addWidget(self.btn_mono)


        self.formLayout_1.setLayout(0, QFormLayout.FieldRole, self.horizontalLayout_2)

        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")

        self.formLayout_1.setWidget(1, QFormLayout.LabelRole, self.label_2)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.edit_gamedir = QComboBox(self.centralwidget)
        self.edit_gamedir.addItem("")
        self.edit_gamedir.addItem("")
        self.edit_gamedir.setObjectName(u"edit_gamedir")
        sizePolicy.setHeightForWidth(self.edit_gamedir.sizePolicy().hasHeightForWidth())
        self.edit_gamedir.setSizePolicy(sizePolicy)
        self.edit_gamedir.setEditable(True)

        self.horizontalLayout_3.addWidget(self.edit_gamedir)

        self.btn_gamedir = QPushButton(self.centralwidget)
        self.btn_gamedir.setObjectName(u"btn_gamedir")

        self.horizontalLayout_3.addWidget(self.btn_gamedir)


        self.formLayout_1.setLayout(1, QFormLayout.FieldRole, self.horizontalLayout_3)

        self.label_3 = QLabel(self.centralwidget)
        self.label_3.setObjectName(u"label_3")

        self.formLayout_1.setWidget(2, QFormLayout.LabelRole, self.label_3)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.edit_savefile = QLineEdit(self.centralwidget)
        self.edit_savefile.setObjectName(u"edit_savefile")

        self.horizontalLayout_4.addWidget(self.edit_savefile)

        self.btn_savefile = QPushButton(self.centralwidget)
        self.btn_savefile.setObjectName(u"btn_savefile")

        self.horizontalLayout_4.addWidget(self.btn_savefile)


        self.formLayout_1.setLayout(2, QFormLayout.FieldRole, self.horizontalLayout_4)


        self.horizontalLayout_1.addLayout(self.formLayout_1)

        self.btn_load = QPushButton(self.centralwidget)
        self.btn_load.setObjectName(u"btn_load")
        sizePolicy1 = QSizePolicy(QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.btn_load.sizePolicy().hasHeightForWidth())
        self.btn_load.setSizePolicy(sizePolicy1)

        self.horizontalLayout_1.addWidget(self.btn_load)

        self.btn_save = QPushButton(self.centralwidget)
        self.btn_save.setObjectName(u"btn_save")
        sizePolicy1.setHeightForWidth(self.btn_save.sizePolicy().hasHeightForWidth())
        self.btn_save.setSizePolicy(sizePolicy1)

        self.horizontalLayout_1.addWidget(self.btn_save)


        self.verticalLayout_1.addLayout(self.horizontalLayout_1)

        self.tabs = QTabWidget(self.centralwidget)
        self.tabs.setObjectName(u"tabs")
        sizePolicy2 = QSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(1)
        sizePolicy2.setHeightForWidth(self.tabs.sizePolicy().hasHeightForWidth())
        self.tabs.setSizePolicy(sizePolicy2)

        self.verticalLayout_1.addWidget(self.tabs)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 750, 22))
        self.menuFile = QMenu(self.menubar)
        self.menuFile.setObjectName(u"menuFile")
        self.menuEdit = QMenu(self.menubar)
        self.menuEdit.setObjectName(u"menuEdit")
        self.menuCopySlot1 = QMenu(self.menuEdit)
        self.menuCopySlot1.setObjectName(u"menuCopySlot1")
        self.menuCopySlot2 = QMenu(self.menuEdit)
        self.menuCopySlot2.setObjectName(u"menuCopySlot2")
        self.menuCopySlot3 = QMenu(self.menuEdit)
        self.menuCopySlot3.setObjectName(u"menuCopySlot3")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())
        self.menuFile.addAction(self.action_import_json)
        self.menuFile.addAction(self.action_export_json)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionExit)
        self.menuEdit.addAction(self.menuCopySlot1.menuAction())
        self.menuEdit.addAction(self.menuCopySlot2.menuAction())
        self.menuEdit.addAction(self.menuCopySlot3.menuAction())
        self.menuCopySlot1.addAction(self.action_slot1to2)
        self.menuCopySlot1.addAction(self.action_slot1to3)
        self.menuCopySlot2.addAction(self.action_slot2to1)
        self.menuCopySlot2.addAction(self.action_slot2to3)
        self.menuCopySlot3.addAction(self.action_slot3to2)
        self.menuCopySlot3.addAction(self.action_slot3to1)

        self.retranslateUi(MainWindow)

        self.edit_mono.setCurrentIndex(2)
        self.tabs.setCurrentIndex(-1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Sundered Save Editor", None))
        self.actionExit.setText(QCoreApplication.translate("MainWindow", u"Exit", None))
        self.action_slot1to2.setText(QCoreApplication.translate("MainWindow", u"to Slot 2", None))
        self.action_slot1to3.setText(QCoreApplication.translate("MainWindow", u"to Slot 3", None))
        self.action_slot2to1.setText(QCoreApplication.translate("MainWindow", u"to Slot 1", None))
        self.action_slot2to3.setText(QCoreApplication.translate("MainWindow", u"to Slot 3", None))
        self.action_slot3to2.setText(QCoreApplication.translate("MainWindow", u"to Slot 1", None))
        self.action_slot3to1.setText(QCoreApplication.translate("MainWindow", u"to Slot 2", None))
        self.action_import_json.setText(QCoreApplication.translate("MainWindow", u"Import JSON...", None))
        self.action_export_json.setText(QCoreApplication.translate("MainWindow", u"Export JSON...", None))
        self.label_1.setText(QCoreApplication.translate("MainWindow", u"Mono executable:", None))
        self.edit_mono.setItemText(0, QCoreApplication.translate("MainWindow", u"C:/Program Files/Mono/bin/mono.exe", None))
        self.edit_mono.setItemText(1, QCoreApplication.translate("MainWindow", u"C:/Program Files (x86)/Mono/bin/mono.exe", None))
        self.edit_mono.setItemText(2, QCoreApplication.translate("MainWindow", u"C:/Program Files (x86)/Mono-2.11.4/bin/mono.exe", None))

        self.btn_mono.setText(QCoreApplication.translate("MainWindow", u"Browse...", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Game directory:", None))
        self.edit_gamedir.setItemText(0, QCoreApplication.translate("MainWindow", u"C:/Program Files/Epic Games/Sundered", None))
        self.edit_gamedir.setItemText(1, QCoreApplication.translate("MainWindow", u"C:/Program Files (x86)/Steam/steamapps/common/Sundered", None))

        self.btn_gamedir.setText(QCoreApplication.translate("MainWindow", u"Browse...", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Save file:", None))
        self.edit_savefile.setText(QCoreApplication.translate("MainWindow", u"%USERPROFILE%/AppData/LocalLow/Thunder Lotus Games/Sundered/SAVE_GAME.sav", None))
        self.btn_savefile.setText(QCoreApplication.translate("MainWindow", u"Browse...", None))
        self.btn_load.setText(QCoreApplication.translate("MainWindow", u"Load", None))
        self.btn_save.setText(QCoreApplication.translate("MainWindow", u"Save", None))
        self.menuFile.setTitle(QCoreApplication.translate("MainWindow", u"File", None))
        self.menuEdit.setTitle(QCoreApplication.translate("MainWindow", u"Edit", None))
        self.menuCopySlot1.setTitle(QCoreApplication.translate("MainWindow", u"Copy Slot 1", None))
        self.menuCopySlot2.setTitle(QCoreApplication.translate("MainWindow", u"Copy Slot 2", None))
        self.menuCopySlot3.setTitle(QCoreApplication.translate("MainWindow", u"Copy Slot 3", None))
    # retranslateUi

