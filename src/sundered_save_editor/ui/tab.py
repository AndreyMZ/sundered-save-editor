# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'Tab.ui'
##
## Created by: Qt User Interface Compiler version 6.7.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QComboBox, QGridLayout,
    QHeaderView, QLabel, QLineEdit, QPushButton,
    QSizePolicy, QSpacerItem, QSpinBox, QToolBox,
    QTreeWidget, QTreeWidgetItem, QVBoxLayout, QWidget)

class Ui_Tab(object):
    def setupUi(self, Tab):
        if not Tab.objectName():
            Tab.setObjectName(u"Tab")
        Tab.resize(548, 361)
        self.verticalLayout_2 = QVBoxLayout(Tab)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.toolBox_1 = QToolBox(Tab)
        self.toolBox_1.setObjectName(u"toolBox_1")
        self.page_1 = QWidget()
        self.page_1.setObjectName(u"page_1")
        self.page_1.setGeometry(QRect(0, 0, 530, 223))
        self.gridLayout_1 = QGridLayout(self.page_1)
        self.gridLayout_1.setObjectName(u"gridLayout_1")
        self.label_3 = QLabel(self.page_1)
        self.label_3.setObjectName(u"label_3")
        sizePolicy = QSizePolicy(QSizePolicy.Policy.Fixed, QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)

        self.gridLayout_1.addWidget(self.label_3, 0, 0, 1, 1)

        self.edit_gameplayTime = QLineEdit(self.page_1)
        self.edit_gameplayTime.setObjectName(u"edit_gameplayTime")
        self.edit_gameplayTime.setReadOnly(True)

        self.gridLayout_1.addWidget(self.edit_gameplayTime, 0, 1, 1, 4)

        self.label_4 = QLabel(self.page_1)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_1.addWidget(self.label_4, 1, 0, 1, 1)

        self.comboBox_difficulty = QComboBox(self.page_1)
        self.comboBox_difficulty.addItem("")
        self.comboBox_difficulty.addItem("")
        self.comboBox_difficulty.addItem("")
        self.comboBox_difficulty.addItem("")
        self.comboBox_difficulty.setObjectName(u"comboBox_difficulty")

        self.gridLayout_1.addWidget(self.comboBox_difficulty, 1, 1, 1, 3)

        self.btn_reset_difficulty = QPushButton(self.page_1)
        self.btn_reset_difficulty.setObjectName(u"btn_reset_difficulty")

        self.gridLayout_1.addWidget(self.btn_reset_difficulty, 1, 4, 1, 1)

        self.label_5 = QLabel(self.page_1)
        self.label_5.setObjectName(u"label_5")
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)

        self.gridLayout_1.addWidget(self.label_5, 2, 0, 1, 1)

        self.spinBox_shards = QSpinBox(self.page_1)
        self.spinBox_shards.setObjectName(u"spinBox_shards")

        self.gridLayout_1.addWidget(self.spinBox_shards, 2, 1, 1, 1)

        self.btn_inc_shards = QPushButton(self.page_1)
        self.btn_inc_shards.setObjectName(u"btn_inc_shards")
        sizePolicy1 = QSizePolicy(QSizePolicy.Policy.Fixed, QSizePolicy.Policy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.btn_inc_shards.sizePolicy().hasHeightForWidth())
        self.btn_inc_shards.setSizePolicy(sizePolicy1)

        self.gridLayout_1.addWidget(self.btn_inc_shards, 2, 2, 2, 1)

        self.btn_dec_shards = QPushButton(self.page_1)
        self.btn_dec_shards.setObjectName(u"btn_dec_shards")
        sizePolicy1.setHeightForWidth(self.btn_dec_shards.sizePolicy().hasHeightForWidth())
        self.btn_dec_shards.setSizePolicy(sizePolicy1)

        self.gridLayout_1.addWidget(self.btn_dec_shards, 2, 3, 2, 1)

        self.btn_reset_shards = QPushButton(self.page_1)
        self.btn_reset_shards.setObjectName(u"btn_reset_shards")
        sizePolicy2 = QSizePolicy(QSizePolicy.Policy.Fixed, QSizePolicy.Policy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.btn_reset_shards.sizePolicy().hasHeightForWidth())
        self.btn_reset_shards.setSizePolicy(sizePolicy2)

        self.gridLayout_1.addWidget(self.btn_reset_shards, 2, 4, 1, 1)

        self.label_6 = QLabel(self.page_1)
        self.label_6.setObjectName(u"label_6")
        sizePolicy.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy)

        self.gridLayout_1.addWidget(self.label_6, 3, 0, 1, 1)

        self.spinBox_totalShardsGathered = QSpinBox(self.page_1)
        self.spinBox_totalShardsGathered.setObjectName(u"spinBox_totalShardsGathered")

        self.gridLayout_1.addWidget(self.spinBox_totalShardsGathered, 3, 1, 1, 1)

        self.btn_reset_totalShardsGathered = QPushButton(self.page_1)
        self.btn_reset_totalShardsGathered.setObjectName(u"btn_reset_totalShardsGathered")
        sizePolicy2.setHeightForWidth(self.btn_reset_totalShardsGathered.sizePolicy().hasHeightForWidth())
        self.btn_reset_totalShardsGathered.setSizePolicy(sizePolicy2)

        self.gridLayout_1.addWidget(self.btn_reset_totalShardsGathered, 3, 4, 1, 1)

        self.label_7 = QLabel(self.page_1)
        self.label_7.setObjectName(u"label_7")
        sizePolicy.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy)

        self.gridLayout_1.addWidget(self.label_7, 4, 0, 1, 1)

        self.spinBox_elderFragments = QSpinBox(self.page_1)
        self.spinBox_elderFragments.setObjectName(u"spinBox_elderFragments")

        self.gridLayout_1.addWidget(self.spinBox_elderFragments, 4, 1, 1, 3)

        self.btn_reset_elderFragments = QPushButton(self.page_1)
        self.btn_reset_elderFragments.setObjectName(u"btn_reset_elderFragments")
        sizePolicy2.setHeightForWidth(self.btn_reset_elderFragments.sizePolicy().hasHeightForWidth())
        self.btn_reset_elderFragments.setSizePolicy(sizePolicy2)

        self.gridLayout_1.addWidget(self.btn_reset_elderFragments, 4, 4, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)

        self.gridLayout_1.addItem(self.verticalSpacer, 5, 0, 1, 5)

        self.toolBox_1.addItem(self.page_1, u"General")
        self.page_2 = QWidget()
        self.page_2.setObjectName(u"page_2")
        self.page_2.setGeometry(QRect(0, 0, 530, 223))
        self.gridLayout_2 = QGridLayout(self.page_2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.label_8 = QLabel(self.page_2)
        self.label_8.setObjectName(u"label_8")
        font = QFont()
        font.setBold(True)
        self.label_8.setFont(font)

        self.gridLayout_2.addWidget(self.label_8, 0, 0, 1, 1)

        self.label_2 = QLabel(self.page_2)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setFont(font)

        self.gridLayout_2.addWidget(self.label_2, 0, 1, 1, 1)

        self.label_9 = QLabel(self.page_2)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setFont(font)

        self.gridLayout_2.addWidget(self.label_9, 0, 2, 1, 1)

        self.ability_shield = QCheckBox(self.page_2)
        self.ability_shield.setObjectName(u"ability_shield")

        self.gridLayout_2.addWidget(self.ability_shield, 1, 0, 1, 1)

        self.ability_shield_corrupted = QCheckBox(self.page_2)
        self.ability_shield_corrupted.setObjectName(u"ability_shield_corrupted")
        self.ability_shield_corrupted.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_shield_corrupted, 1, 1, 1, 1)

        self.ability_resist_roll = QCheckBox(self.page_2)
        self.ability_resist_roll.setObjectName(u"ability_resist_roll")

        self.gridLayout_2.addWidget(self.ability_resist_roll, 1, 2, 1, 1)

        self.ability_doublejump = QCheckBox(self.page_2)
        self.ability_doublejump.setObjectName(u"ability_doublejump")

        self.gridLayout_2.addWidget(self.ability_doublejump, 2, 0, 1, 1)

        self.ability_doublejump_corrupted = QCheckBox(self.page_2)
        self.ability_doublejump_corrupted.setObjectName(u"ability_doublejump_corrupted")
        self.ability_doublejump_corrupted.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_doublejump_corrupted, 2, 1, 1, 1)

        self.ability_resist_attack = QCheckBox(self.page_2)
        self.ability_resist_attack.setObjectName(u"ability_resist_attack")
        self.ability_resist_attack.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_resist_attack, 2, 2, 1, 1)

        self.ability_gun = QCheckBox(self.page_2)
        self.ability_gun.setObjectName(u"ability_gun")

        self.gridLayout_2.addWidget(self.ability_gun, 3, 0, 1, 1)

        self.ability_gun_corrupted = QCheckBox(self.page_2)
        self.ability_gun_corrupted.setObjectName(u"ability_gun_corrupted")
        self.ability_gun_corrupted.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_gun_corrupted, 3, 1, 1, 1)

        self.ability_resist_crit = QCheckBox(self.page_2)
        self.ability_resist_crit.setObjectName(u"ability_resist_crit")
        self.ability_resist_crit.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_resist_crit, 3, 2, 1, 1)

        self.ability_smashattack = QCheckBox(self.page_2)
        self.ability_smashattack.setObjectName(u"ability_smashattack")

        self.gridLayout_2.addWidget(self.ability_smashattack, 4, 0, 1, 1)

        self.ability_smashattack_corrupted = QCheckBox(self.page_2)
        self.ability_smashattack_corrupted.setObjectName(u"ability_smashattack_corrupted")
        self.ability_smashattack_corrupted.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_smashattack_corrupted, 4, 1, 1, 1)

        self.ability_resist_slam = QCheckBox(self.page_2)
        self.ability_resist_slam.setObjectName(u"ability_resist_slam")
        self.ability_resist_slam.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_resist_slam, 4, 2, 1, 1)

        self.ability_techdash = QCheckBox(self.page_2)
        self.ability_techdash.setObjectName(u"ability_techdash")

        self.gridLayout_2.addWidget(self.ability_techdash, 5, 0, 1, 1)

        self.ability_techdash_corrupted = QCheckBox(self.page_2)
        self.ability_techdash_corrupted.setObjectName(u"ability_techdash_corrupted")
        self.ability_techdash_corrupted.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_techdash_corrupted, 5, 1, 1, 1)

        self.ability_resist_perk = QCheckBox(self.page_2)
        self.ability_resist_perk.setObjectName(u"ability_resist_perk")
        self.ability_resist_perk.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_resist_perk, 5, 2, 1, 1)

        self.ability_wallrun = QCheckBox(self.page_2)
        self.ability_wallrun.setObjectName(u"ability_wallrun")

        self.gridLayout_2.addWidget(self.ability_wallrun, 6, 0, 1, 1)

        self.ability_wallrun_corrupted = QCheckBox(self.page_2)
        self.ability_wallrun_corrupted.setObjectName(u"ability_wallrun_corrupted")
        self.ability_wallrun_corrupted.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_wallrun_corrupted, 6, 1, 1, 1)

        self.ability_resist_jump = QCheckBox(self.page_2)
        self.ability_resist_jump.setObjectName(u"ability_resist_jump")
        self.ability_resist_jump.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_resist_jump, 6, 2, 1, 1)

        self.ability_hookshot = QCheckBox(self.page_2)
        self.ability_hookshot.setObjectName(u"ability_hookshot")

        self.gridLayout_2.addWidget(self.ability_hookshot, 7, 0, 1, 1)

        self.ability_hookshot_corrupted = QCheckBox(self.page_2)
        self.ability_hookshot_corrupted.setObjectName(u"ability_hookshot_corrupted")
        self.ability_hookshot_corrupted.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_hookshot_corrupted, 7, 1, 1, 1)

        self.ability_resist_gun = QCheckBox(self.page_2)
        self.ability_resist_gun.setObjectName(u"ability_resist_gun")
        self.ability_resist_gun.setEnabled(False)

        self.gridLayout_2.addWidget(self.ability_resist_gun, 7, 2, 1, 1)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)

        self.gridLayout_2.addItem(self.verticalSpacer_2, 8, 0, 1, 1)

        self.toolBox_1.addItem(self.page_2, u"Abilities")
        self.page_3 = QWidget()
        self.page_3.setObjectName(u"page_3")
        self.page_3.setGeometry(QRect(0, 0, 101, 122))
        self.verticalLayout_3 = QVBoxLayout(self.page_3)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.ability_stinger = QCheckBox(self.page_3)
        self.ability_stinger.setObjectName(u"ability_stinger")

        self.verticalLayout_3.addWidget(self.ability_stinger)

        self.ability_negociator = QCheckBox(self.page_3)
        self.ability_negociator.setObjectName(u"ability_negociator")

        self.verticalLayout_3.addWidget(self.ability_negociator)

        self.ability_warlord = QCheckBox(self.page_3)
        self.ability_warlord.setObjectName(u"ability_warlord")

        self.verticalLayout_3.addWidget(self.ability_warlord)

        self.ability_negator = QCheckBox(self.page_3)
        self.ability_negator.setObjectName(u"ability_negator")

        self.verticalLayout_3.addWidget(self.ability_negator)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_3)

        self.toolBox_1.addItem(self.page_3, u"Additional abilities")
        self.page_4 = QWidget()
        self.page_4.setObjectName(u"page_4")
        self.page_4.setGeometry(QRect(0, 0, 98, 72))
        self.verticalLayout_4 = QVBoxLayout(self.page_4)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 1, 0, 0)
        self.perkTree = QTreeWidget(self.page_4)
        __qtreewidgetitem = QTreeWidgetItem()
        __qtreewidgetitem.setTextAlignment(5, Qt.AlignCenter);
        __qtreewidgetitem.setTextAlignment(4, Qt.AlignCenter);
        __qtreewidgetitem.setTextAlignment(3, Qt.AlignCenter);
        __qtreewidgetitem.setTextAlignment(2, Qt.AlignCenter);
        __qtreewidgetitem.setTextAlignment(1, Qt.AlignCenter);
        __qtreewidgetitem.setText(0, u"Name");
        self.perkTree.setHeaderItem(__qtreewidgetitem)
        QTreeWidgetItem(self.perkTree)
        __qtreewidgetitem1 = QTreeWidgetItem(self.perkTree)
        QTreeWidgetItem(__qtreewidgetitem1)
        QTreeWidgetItem(__qtreewidgetitem1)
        QTreeWidgetItem(__qtreewidgetitem1)
        QTreeWidgetItem(__qtreewidgetitem1)
        QTreeWidgetItem(__qtreewidgetitem1)
        QTreeWidgetItem(__qtreewidgetitem1)
        QTreeWidgetItem(__qtreewidgetitem1)
        QTreeWidgetItem(__qtreewidgetitem1)
        __qtreewidgetitem2 = QTreeWidgetItem(self.perkTree)
        QTreeWidgetItem(__qtreewidgetitem2)
        QTreeWidgetItem(__qtreewidgetitem2)
        QTreeWidgetItem(__qtreewidgetitem2)
        QTreeWidgetItem(__qtreewidgetitem2)
        QTreeWidgetItem(__qtreewidgetitem2)
        QTreeWidgetItem(__qtreewidgetitem2)
        QTreeWidgetItem(__qtreewidgetitem2)
        QTreeWidgetItem(__qtreewidgetitem2)
        QTreeWidgetItem(__qtreewidgetitem2)
        QTreeWidgetItem(__qtreewidgetitem2)
        QTreeWidgetItem(__qtreewidgetitem2)
        __qtreewidgetitem3 = QTreeWidgetItem(self.perkTree)
        QTreeWidgetItem(__qtreewidgetitem3)
        QTreeWidgetItem(__qtreewidgetitem3)
        QTreeWidgetItem(__qtreewidgetitem3)
        QTreeWidgetItem(__qtreewidgetitem3)
        QTreeWidgetItem(__qtreewidgetitem3)
        QTreeWidgetItem(__qtreewidgetitem3)
        QTreeWidgetItem(__qtreewidgetitem3)
        QTreeWidgetItem(__qtreewidgetitem3)
        QTreeWidgetItem(__qtreewidgetitem3)
        QTreeWidgetItem(__qtreewidgetitem3)
        self.perkTree.setObjectName(u"perkTree")
        self.perkTree.header().setMinimumSectionSize(64)
        self.perkTree.header().setStretchLastSection(False)

        self.verticalLayout_4.addWidget(self.perkTree)

        self.toolBox_1.addItem(self.page_4, u"Perks")

        self.verticalLayout_2.addWidget(self.toolBox_1)


        self.retranslateUi(Tab)

        self.toolBox_1.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Tab)
    # setupUi

    def retranslateUi(self, Tab):
        Tab.setWindowTitle(QCoreApplication.translate("Tab", u"Form", None))
        self.label_3.setText(QCoreApplication.translate("Tab", u"Gameplay time:", None))
        self.label_4.setText(QCoreApplication.translate("Tab", u"Difficulty:", None))
        self.comboBox_difficulty.setItemText(0, QCoreApplication.translate("Tab", u"Easy", None))
        self.comboBox_difficulty.setItemText(1, QCoreApplication.translate("Tab", u"Normal", None))
        self.comboBox_difficulty.setItemText(2, QCoreApplication.translate("Tab", u"Hard", None))
        self.comboBox_difficulty.setItemText(3, QCoreApplication.translate("Tab", u"Insane", None))

        self.btn_reset_difficulty.setText(QCoreApplication.translate("Tab", u"Reset", None))
        self.label_5.setText(QCoreApplication.translate("Tab", u"Shards:", None))
        self.btn_inc_shards.setText(QCoreApplication.translate("Tab", u"+1000", None))
        self.btn_dec_shards.setText(QCoreApplication.translate("Tab", u"-1000", None))
        self.btn_reset_shards.setText(QCoreApplication.translate("Tab", u"Reset", None))
        self.label_6.setText(QCoreApplication.translate("Tab", u"Total shards gathered:", None))
        self.btn_reset_totalShardsGathered.setText(QCoreApplication.translate("Tab", u"Reset", None))
        self.label_7.setText(QCoreApplication.translate("Tab", u"Elder fragments:", None))
        self.btn_reset_elderFragments.setText(QCoreApplication.translate("Tab", u"Reset", None))
        self.toolBox_1.setItemText(self.toolBox_1.indexOf(self.page_1), QCoreApplication.translate("Tab", u"General", None))
        self.label_8.setText(QCoreApplication.translate("Tab", u"Valkyrie Abilities", None))
        self.label_2.setText(QCoreApplication.translate("Tab", u"Corrupted Abilities", None))
        self.label_9.setText(QCoreApplication.translate("Tab", u"Resist Abilities", None))
#if QT_CONFIG(tooltip)
        self.ability_shield.setToolTip(QCoreApplication.translate("Tab", u"Protects you from incoming damage. Slowly regenerates if no damage is taken over time.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_shield.setText(QCoreApplication.translate("Tab", u"Deflecting Shield", None))
#if QT_CONFIG(tooltip)
        self.ability_shield_corrupted.setToolTip(QCoreApplication.translate("Tab", u"Absorbs damage, which is then reflected upon your enemies.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_shield_corrupted.setText(QCoreApplication.translate("Tab", u"Mi-Go Shield", None))
#if QT_CONFIG(tooltip)
        self.ability_resist_roll.setToolTip(QCoreApplication.translate("Tab", u"Rolling on the ground damages your enemies.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_resist_roll.setText(QCoreApplication.translate("Tab", u"Otaloth's Demise (Dodge Attack)", None))
#if QT_CONFIG(tooltip)
        self.ability_doublejump.setToolTip(QCoreApplication.translate("Tab", u"Grants a second jump while airborne.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_doublejump.setText(QCoreApplication.translate("Tab", u"Leaping Device", None))
#if QT_CONFIG(tooltip)
        self.ability_doublejump_corrupted.setToolTip(QCoreApplication.translate("Tab", u"Gives you the power to glide while airborne.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_doublejump_corrupted.setText(QCoreApplication.translate("Tab", u"Z'Toggua's Wings", None))
#if QT_CONFIG(tooltip)
        self.ability_resist_attack.setToolTip(QCoreApplication.translate("Tab", u"Combo attacks deal extra damage.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_resist_attack.setText(QCoreApplication.translate("Tab", u"Chol's Despair (Combo++)", None))
#if QT_CONFIG(tooltip)
        self.ability_gun.setToolTip(QCoreApplication.translate("Tab", u"A powerful weapon, with tremendous recoil. Its bullets pierce enemies and obstacles.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_gun.setText(QCoreApplication.translate("Tab", u"Valkyrie Cannon", None))
#if QT_CONFIG(tooltip)
        self.ability_gun_corrupted.setToolTip(QCoreApplication.translate("Tab", u"A charged beam of eldritch energy. Its blaze pierces everything.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_gun_corrupted.setText(QCoreApplication.translate("Tab", u"Armageddon", None))
#if QT_CONFIG(tooltip)
        self.ability_resist_crit.setToolTip(QCoreApplication.translate("Tab", u"Melee Critical Hits extend further and deal extra Damage.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_resist_crit.setText(QCoreApplication.translate("Tab", u"Dominion's Curse (Crit++)", None))
#if QT_CONFIG(tooltip)
        self.ability_smashattack.setToolTip(QCoreApplication.translate("Tab", u"A charged melee attack of colossal strength that annihilates obstacles and enemies.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_smashattack.setText(QCoreApplication.translate("Tab", u"Strength Amplifier", None))
#if QT_CONFIG(tooltip)
        self.ability_smashattack_corrupted.setToolTip(QCoreApplication.translate("Tab", u"A heavy melee attack that can be charged longer to smash your enemies with the force of the Elder Gods.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_smashattack_corrupted.setText(QCoreApplication.translate("Tab", u"Ghatanothoa's Strength", None))
#if QT_CONFIG(tooltip)
        self.ability_resist_slam.setToolTip(QCoreApplication.translate("Tab", u"Your downward air attacks deal extra Damage.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_resist_slam.setText(QCoreApplication.translate("Tab", u"Yi-Bot's Flaw (Downslam++)", None))
#if QT_CONFIG(tooltip)
        self.ability_techdash.setToolTip(QCoreApplication.translate("Tab", u"A lightning-fast dash that can be used while airborne.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_techdash.setText(QCoreApplication.translate("Tab", u"Propulsion Engine", None))
#if QT_CONFIG(tooltip)
        self.ability_techdash_corrupted.setToolTip(QCoreApplication.translate("Tab", u"An eldritch dash in any direction that can be used while airborne.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_techdash_corrupted.setText(QCoreApplication.translate("Tab", u"Azathoth's Breath", None))
#if QT_CONFIG(tooltip)
        self.ability_resist_perk.setToolTip(QCoreApplication.translate("Tab", u"Extra perk slot.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_resist_perk.setText(QCoreApplication.translate("Tab", u"Sael-Yot's Demise (Extra Perk)", None))
#if QT_CONFIG(tooltip)
        self.ability_wallrun.setToolTip(QCoreApplication.translate("Tab", u"Climb up walls from a running start.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_wallrun.setText(QCoreApplication.translate("Tab", u"Gravitational Boots", None))
#if QT_CONFIG(tooltip)
        self.ability_wallrun_corrupted.setToolTip(QCoreApplication.translate("Tab", u"Morphs you into a eight legged beast, gifting you the ability to navigate walls more freely.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_wallrun_corrupted.setText(QCoreApplication.translate("Tab", u"Atlach-Nacha's Grip", None))
#if QT_CONFIG(tooltip)
        self.ability_resist_jump.setToolTip(QCoreApplication.translate("Tab", u"Grants a third jump while airborne.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_resist_jump.setText(QCoreApplication.translate("Tab", u"Salvation's Bane (Triple Jump)", None))
#if QT_CONFIG(tooltip)
        self.ability_hookshot.setToolTip(QCoreApplication.translate("Tab", u"Latch onto hooks or enemies to violently propel yourself into the air.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_hookshot.setText(QCoreApplication.translate("Tab", u"Grappling Hook", None))
#if QT_CONFIG(tooltip)
        self.ability_hookshot_corrupted.setToolTip(QCoreApplication.translate("Tab", u"Latch onto hooks or enemies to violently propel yourself into the air. A second explosion propels you further.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_hookshot_corrupted.setText(QCoreApplication.translate("Tab", u"Eldritch Pull", None))
#if QT_CONFIG(tooltip)
        self.ability_resist_gun.setToolTip(QCoreApplication.translate("Tab", u"Drastically increases the Cannon's Damage.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_resist_gun.setText(QCoreApplication.translate("Tab", u"The Purifier (GunBullet+++)", None))
        self.toolBox_1.setItemText(self.toolBox_1.indexOf(self.page_2), QCoreApplication.translate("Tab", u"Abilities", None))
#if QT_CONFIG(tooltip)
        self.ability_stinger.setToolTip(QCoreApplication.translate("Tab", u"Attack immediately after rolling to perform a powerful forward lunge.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_stinger.setText(QCoreApplication.translate("Tab", u"Stinger", None))
#if QT_CONFIG(tooltip)
        self.ability_negociator.setToolTip(QCoreApplication.translate("Tab", u"The cost of every upgrade has been reduced by 20%.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_negociator.setText(QCoreApplication.translate("Tab", u"Negociator", None))
#if QT_CONFIG(tooltip)
        self.ability_warlord.setToolTip(QCoreApplication.translate("Tab", u"Your Finishers' area of effect is now 50% larger and their damage has increased.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_warlord.setText(QCoreApplication.translate("Tab", u"Warlord", None))
#if QT_CONFIG(tooltip)
        self.ability_negator.setToolTip(QCoreApplication.translate("Tab", u"You are now immune to the wind's pushing power and take 20% reduced damage from environment obstacles.", None))
#endif // QT_CONFIG(tooltip)
        self.ability_negator.setText(QCoreApplication.translate("Tab", u"Negator", None))
        self.toolBox_1.setItemText(self.toolBox_1.indexOf(self.page_3), QCoreApplication.translate("Tab", u"Additional abilities", None))
        ___qtreewidgetitem = self.perkTree.headerItem()
        ___qtreewidgetitem.setText(5, QCoreApplication.translate("Tab", u"Rank 4", None));
        ___qtreewidgetitem.setText(4, QCoreApplication.translate("Tab", u"Rank 3", None));
        ___qtreewidgetitem.setText(3, QCoreApplication.translate("Tab", u"Rank 2", None));
        ___qtreewidgetitem.setText(2, QCoreApplication.translate("Tab", u"Rank 1", None));
        ___qtreewidgetitem.setText(1, QCoreApplication.translate("Tab", u"No", None));

        __sortingEnabled = self.perkTree.isSortingEnabled()
        self.perkTree.setSortingEnabled(False)
        ___qtreewidgetitem1 = self.perkTree.topLevelItem(0)
        ___qtreewidgetitem1.setText(0, QCoreApplication.translate("Tab", u"All", None));
        ___qtreewidgetitem2 = self.perkTree.topLevelItem(1)
        ___qtreewidgetitem2.setText(0, QCoreApplication.translate("Tab", u"Offensive Perks", None));
        ___qtreewidgetitem3 = ___qtreewidgetitem2.child(0)
        ___qtreewidgetitem3.setText(0, QCoreApplication.translate("Tab", u"Berzerk", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem3.setToolTip(2, QCoreApplication.translate("Tab", u"Advantage: Defeating an enemy gives a stackable 8/10/12/15% Damage boost, up to a maximum of 5 stacks.\n"
"Disadvantage: Reduces your armor by -12/16/20/20.", None));
        ___qtreewidgetitem3.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Defeating an enemy gives a stackable 8/10/12/15% Damage boost, up to a maximum of 5 stacks.\n"
"Disadvantage: Reduces your armor by -12/16/20/20.\n"
"Note: Stacks last for 8 seconds.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem4 = ___qtreewidgetitem2.child(1)
        ___qtreewidgetitem4.setText(0, QCoreApplication.translate("Tab", u"Sharp Edge", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem4.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Increases Melee Damage by 15/20/25/30%.\n"
"Disadvantage: Decreases your Health by 20/25/30/35%.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem5 = ___qtreewidgetitem2.child(2)
        ___qtreewidgetitem5.setText(0, QCoreApplication.translate("Tab", u"Assassin", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem5.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Increases the chance of landing a Critical Hit by 10/15/20/30%.\n"
"Disadvantage: Reduces Health by 20/25/30/40%.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem6 = ___qtreewidgetitem2.child(3)
        ___qtreewidgetitem6.setText(0, QCoreApplication.translate("Tab", u"Bullet Transfabricator", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem6.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Ammo regenerates every 30/15/5/5 seconds.\n"
"Disadvantage: Reduces your Melee Damage by 25/35/50/25%.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem7 = ___qtreewidgetitem2.child(4)
        ___qtreewidgetitem7.setText(0, QCoreApplication.translate("Tab", u"Shield Piercer", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem7.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Your attacks ignore enemy Shields.\n"
"Disadvantage: Lowers your Shield Regeneration rate by 50/40/30/20%.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem8 = ___qtreewidgetitem2.child(5)
        ___qtreewidgetitem8.setText(0, QCoreApplication.translate("Tab", u"High Density Bullets", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem8.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Increases your Cannon's Damage by 20/30/40/60%.\n"
"Disadvantage: Reduces the Cannon's Ammo Capacity by -2/3/4/4.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem9 = ___qtreewidgetitem2.child(6)
        ___qtreewidgetitem9.setText(0, QCoreApplication.translate("Tab", u"Executioner", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem9.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Increases your Critical Hit Damage by 250/400/600/950%.\n"
"Disadvantage: Reduces your Melee and Cannon Damage by 30/40/50/60%.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem10 = ___qtreewidgetitem2.child(7)
        ___qtreewidgetitem10.setText(0, QCoreApplication.translate("Tab", u"Deathblow", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem10.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Increases your finisher's damage by 25/50/75/100%.\n"
"Disadvantage: Reduces you maximum energy by -1/2/3/4.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem11 = self.perkTree.topLevelItem(2)
        ___qtreewidgetitem11.setText(0, QCoreApplication.translate("Tab", u"Defensive Perks", None));
        ___qtreewidgetitem12 = ___qtreewidgetitem11.child(0)
        ___qtreewidgetitem12.setText(0, QCoreApplication.translate("Tab", u"Life Absorb", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem12.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Heal 1/2/5/8% of your max Health every time you kill an enemy.\n"
"Disadvantage: Health Elixir drop 50/75/100/100% less often.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem13 = ___qtreewidgetitem11.child(1)
        ___qtreewidgetitem13.setText(0, QCoreApplication.translate("Tab", u"Shield Absorb", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem13.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Regain 10/15/20/40% of your max Shield every time you kill an enemy.\n"
"Disadvantage: Increases your Shield Downtime by 1/1,75/2,5/4s.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem14 = ___qtreewidgetitem11.child(2)
        ___qtreewidgetitem14.setText(0, QCoreApplication.translate("Tab", u"Shield Converter", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem14.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: 40/50/60/70% of your maximum Health is converted into Shield.\n"
"Disadvantage: Your maximum Health becomes 1.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem15 = ___qtreewidgetitem11.child(3)
        ___qtreewidgetitem15.setText(0, QCoreApplication.translate("Tab", u"Shield Overdrive", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem15.setToolTip(1, QCoreApplication.translate("Tab", u"Advantage: Your Shield constantly regenerates.\n"
"Disadvantage: Lowers Shield Regeneration rate by 80/70/60/50%.\n"
"Note: If you use Shield Overdrive and Shield Absorb, downtime from Absorb is added after Overdrive apply. Thus, if you use both Perks at Rank 1, you still have 1 second downtime.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem16 = ___qtreewidgetitem11.child(4)
        ___qtreewidgetitem16.setText(0, QCoreApplication.translate("Tab", u"Tough Skin", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem16.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Increases your Armor by 10/12/14/18.\n"
"Disadvantage: Reduces the Cannon's Damage by 25/30/35/40%.\n"
"Note: Tough Skin is affected by Dominon's Guard Skill. At Rank 3, you gain 19 Armor with both active.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem17 = ___qtreewidgetitem11.child(5)
        ___qtreewidgetitem17.setText(0, QCoreApplication.translate("Tab", u"Stand Firm", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem17.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Reduces Cannon recoil and incoming environmental damage by 30/40/50/60%.\n"
"Disadvantage: None.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem18 = ___qtreewidgetitem11.child(6)
        ___qtreewidgetitem18.setText(0, QCoreApplication.translate("Tab", u"Shield Flasks", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem18.setToolTip(1, QCoreApplication.translate("Tab", u"Advantage: Increases your maximum number of Health Elixirs by 2/3/4/6.\n"
"Disadvantage: Transform Health Elixirs into Shield Elixirs.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem19 = ___qtreewidgetitem11.child(7)
        ___qtreewidgetitem19.setText(0, QCoreApplication.translate("Tab", u"Harvester", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem19.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Regain 2/3/5/7% of your Damage as Health every time you hit an enemy.\n"
"Disadvantage: Reduces your armor by 15/20/25/30.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem20 = ___qtreewidgetitem11.child(8)
        ___qtreewidgetitem20.setText(0, QCoreApplication.translate("Tab", u"Shield Drain", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem20.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Regain 2/3/4/5% of your Damage as Shield every time you hit an enemy.\n"
"Disadvantage: Reduces your maximum Shield by 30/40/50/50%.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem21 = ___qtreewidgetitem11.child(9)
        ___qtreewidgetitem21.setText(0, QCoreApplication.translate("Tab", u"Rebirth", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem21.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Escape death once during a run.\n"
"Disadvantage: Enemies drop 50/38/25/0% fewer Shards.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem22 = ___qtreewidgetitem11.child(10)
        ___qtreewidgetitem22.setText(0, QCoreApplication.translate("Tab", u"Steel Spine", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem22.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Increases your Armor by 20/25/30/45.\n"
"Disadvantage: Reduces your Health and Shield by 30/35/40/50%.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem23 = self.perkTree.topLevelItem(3)
        ___qtreewidgetitem23.setText(0, QCoreApplication.translate("Tab", u"Utility Perks", None));
        ___qtreewidgetitem24 = ___qtreewidgetitem23.child(0)
        ___qtreewidgetitem24.setText(0, QCoreApplication.translate("Tab", u"Blind Luck", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem24.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Increases Luck by 20/25/30/40.\n"
"Disadvantage: The small rooms normally appearing on your mini map will not show.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem25 = ___qtreewidgetitem23.child(1)
        ___qtreewidgetitem25.setText(0, QCoreApplication.translate("Tab", u"Shard Collector", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem25.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: You will gain a few/small/significant/important amount of shards after landing a Critical Hit.\n"
"Disadvantage: None.\n"
"Note: Does not increase Critical Hit Chance.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem26 = ___qtreewidgetitem23.child(2)
        ___qtreewidgetitem26.setText(0, QCoreApplication.translate("Tab", u"Monstrous Lure", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem26.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Corrupted treasures drop 30/45/60/150% more Shards.\n"
"Disadvantage: Hordes spawn 2/2.5/3/5 times more frequently.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem27 = ___qtreewidgetitem23.child(3)
        ___qtreewidgetitem27.setText(0, QCoreApplication.translate("Tab", u"Golden Charm", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem27.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Regular monsters drop Shards 20/30/40/60% more often.\n"
"Disadvantage: None.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem28 = ___qtreewidgetitem23.child(4)
        ___qtreewidgetitem28.setText(0, QCoreApplication.translate("Tab", u"Health Charm", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem28.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Monsters have a 4/6/8/8% chance to convert their Shards into an Health Elixir.\n"
"Disadvantage: Enemies drop 15/20/25/0% fewer Shards.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem29 = ___qtreewidgetitem23.child(5)
        ___qtreewidgetitem29.setText(0, QCoreApplication.translate("Tab", u"Magnet", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem29.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Attracts drops from 3/4/5/8 times farther.\n"
"Disadvantage: None.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem30 = ___qtreewidgetitem23.child(6)
        ___qtreewidgetitem30.setText(0, QCoreApplication.translate("Tab", u"Nemesis", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem30.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Bosses, Minibosses and Lith' enemies drop 50/125/200/300% more Shards.\n"
"Disadvantage: Bosses, Minibosses and Lith' enemies are 40/75/100/150% stronger.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem31 = ___qtreewidgetitem23.child(7)
        ___qtreewidgetitem31.setText(0, QCoreApplication.translate("Tab", u"Energy Absorb", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem31.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Gain a 40/60/80/100% chance to regain Energy every time you land a Critical Hit on an enemy. Gain 5/5/5/10% chance to land a Critical Hit.\n"
"Disadvantage: None.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem32 = ___qtreewidgetitem23.child(8)
        ___qtreewidgetitem32.setText(0, QCoreApplication.translate("Tab", u"Gunslinger", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem32.setToolTip(0, QCoreApplication.translate("Tab", u"Advantage: Gain 1 Ammo every time you unleash a finisher.\n"
"Disadvantage: Increases the number of Charges needed before you can unleash a finisher by 6/4/2/0.", None));
#endif // QT_CONFIG(tooltip)
        ___qtreewidgetitem33 = ___qtreewidgetitem23.child(9)
        ___qtreewidgetitem33.setText(0, QCoreApplication.translate("Tab", u"Eradicator", None));
#if QT_CONFIG(tooltip)
        ___qtreewidgetitem33.setToolTip(1, QCoreApplication.translate("Tab", u"Advantage: Eradicator will cause tier 4 Perks to appear.\n"
"Disadvantage: Lowers your Armor by -30 and your Damage by 30%. Stronger foes will shadow your every step.\n"
"Note: This perk starts at rank 4.", None));
#endif // QT_CONFIG(tooltip)
        self.perkTree.setSortingEnabled(__sortingEnabled)

        self.toolBox_1.setItemText(self.toolBox_1.indexOf(self.page_4), QCoreApplication.translate("Tab", u"Perks", None))
    # retranslateUi

