from enum import Enum


class PerkOffensive(Enum):
	# 1. Offensive Perks
	perk_berzerk        = "PERK_BERZERK"        # 1.1	Berzerk
	perk_sharp_edge     = "PERK_SHARP_EDGE"     # 1.2	Sharp Edge
	perk_assassin       = "PERK_ASSASSIN"       # 1.3	Assassin
	perk_ammo_regen     = "PERK_AMMO_REGEN"     # 1.4	Bullet Transfabricator
	perk_shield_piercer = "PERK_SHIELD_PIERCER" # 1.5	Shield Piercer
	perk_ammo_pack      = "PERK_AMMO_PACK"      # 1.6	High Density Bullets
	perk_executioner    = "PERK_EXECUTIONER"    # 1.7	Executioner
	perk_reckless_power = "PERK_RECKLESS_POWER" # 1.8	Deathblow

class PerkDefensive(Enum):
	# 2. Defensive Perks
	perk_life_absorb      = "PERK_LIFE_ABSORB"      # 2.1	Life Absorb
	perk_shield_absorb    = "PERK_SHIELD_ABSORB"    # 2.2	Shield Absorb
	perk_shield_converter = "PERK_SHIELD_CONVERTER" # 2.3	Shield Converter
	perk_shield_overdrive = "PERK_SHIELD_OVERDRIVE" # 2.4	Shield Overdrive
	perk_armor_enhancer   = "PERK_ARMOR_ENHANCER"   # 2.5	Tough Skin
	perk_gun_knockback    = "PERK_GUN_KNOCKBACK"    # 2.6	Stand Firm
	perk_shield_estus     = "PERK_SHIELD_ESTUS"     # 2.7	Shield Flasks
	perk_life_drain       = "PERK_LIFE_DRAIN"       # 2.8	Harvester
	perk_shield_drain     = "PERK_SHIELD_DRAIN"     # 2.9	Shield Drain
	perk_extra_life       = "PERK_EXTRA_LIFE"       # 2.10	Rebirth
	perk_legendary_armor  = "PERK_LEGENDARY_ARMOR"  # 2.11	Steel Spine

class PerkUtility(Enum):
	# 3. Utility Perks
	perk_blind_man_luck     = "PERK_BLIND_MAN_LUCK"     # 3.1	Blind Luck
	perk_bounty_hunter      = "PERK_BOUNTY_HUNTER"      # 3.2	Shard Collector
	perk_monster_lure       = "PERK_MONSTER_LURE"       # 3.3	Monstrous Lure
	perk_golden_charm       = "PERK_GOLDEN_CHARM"       # 3.4	Golden Charm
	perk_health_charm       = "PERK_HEALTH_CHARM"       # 3.5	Health Charm
	perk_magnet             = "PERK_MAGNET"             # 3.6	Magnet
	perk_nemesis            = "PERK_NEMESIS"            # 3.7	Nemesis
	perk_stamina_absorb     = "PERK_STAMINA_ABSORB"     # 3.8	Energy Absorb
	perk_bullet_temporizer  = "PERK_BULLET_TEMPORIZER"  # 3.9	Gunslinger
	perk_herald_of_the_gong = "PERK_HERALD_OF_THE_GONG" # 3.10	Eradicator
