from typing import TypedDict


class PerkData(TypedDict):
	_activePerks: list[str]
	_availablePerks: list[str]
	_unseenPerks: list[str]

class UpgradeSystem(TypedDict):
	upgradeValues: dict[str, dict[str, int]]

class SaveGame(TypedDict):
	difficulty: str
	gameplayTime: float
	shards: int
	totalShardsGathered: int
	elderFragments: int
	perkData: PerkData
	upgradeSystem: UpgradeSystem
	discoveredRooms: list[int]
	playedOnceEverVO: list[int]
	flags: list[str]

SaveSlots = list[SaveGame]
