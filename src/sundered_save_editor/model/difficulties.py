from enum import Enum


class Difficulty(Enum):
	easy   = "EASY"
	normal = "NORMAL"
	hard   = "HARD"
	insane = "INSANE"
