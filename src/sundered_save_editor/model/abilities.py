from enum import Enum


class Ability(Enum):
	ability_shield      = "ABILITY_SHIELD"
	ability_doublejump  = "ABILITY_DOUBLEJUMP"
	ability_gun         = "ABILITY_GUN"
	ability_smashattack = "ABILITY_SMASHATTACK"
	ability_techdash    = "ABILITY_TECHDASH"
	ability_wallrun     = "ABILITY_WALLRUN"
	ability_hookshot    = "ABILITY_HOOKSHOT"

class AbilityCorrupted(Enum):
	ability_shield_corrupted      = "ABILITY_SHIELD_CORRUPTED"
	ability_doublejump_corrupted  = "ABILITY_DOUBLEJUMP_CORRUPTED"
	ability_gun_corrupted         = "ABILITY_GUN_CORRUPTED"
	ability_smashattack_corrupted = "ABILITY_SMASHATTACK_CORRUPTED"
	ability_techdash_corrupted    = "ABILITY_TECHDASH_CORRUPTED"
	ability_wallrun_corrupted     = "ABILITY_WALLRUN_CORRUPTED"
	ability_hookshot_corrupted    = "ABILITY_HOOKSHOT_CORRUPTED"

class AbilityResist(Enum):
	ability_resist_roll   = ("RESIST_CORRUPTED_SHARD_DESTROYED_01", "UPGRADE_L_RESIST_1_DODGE",        "RESIST-[Big]-1-DodgeAttack",  "ABILITY_RESIST_ROLL")
	ability_resist_attack = ("RESIST_CORRUPTED_SHARD_DESTROYED_02", "UPGRADE_L_RESIST_2_COMBOS",       "RESIST-[Big]-2-Combo++",      "ABILITY_RESIST_ATTACK")
	ability_resist_crit   = ("RESIST_CORRUPTED_SHARD_DESTROYED_03", "UPGRADE_L_RESIST_3_CRIT",         "RESIST-[Big]-3-Crit++",       "ABILITY_RESIST_CRIT")
	ability_resist_slam   = ("RESIST_CORRUPTED_SHARD_DESTROYED_04", "UPGRADE_L_RESIST_4_DOWNSLAM",     "RESIST-[Big]-4-Downslam++",   "ABILITY_RESIST_SLAM")
	ability_resist_perk   = ("RESIST_CORRUPTED_SHARD_DESTROYED_05", "UPGRADE_L_RESIST_5_PERK",         "RESIST-[Big]-5-ExtraPerk",    None)
	ability_resist_jump   = ("RESIST_CORRUPTED_SHARD_DESTROYED_06", "UPGRADE_L_RESIST_6_TRIPLE_JUMP",  "RESIST-[Big]-6-TripleJump",   "ABILITY_RESIST_JUMP")
	ability_resist_gun    = ("RESIST_CORRUPTED_SHARD_DESTROYED_07", "UPGRADE_L_RESIST_7_IMPROVED_GUN", "RESIST-[Big]-7-GunBullet+++", "ABILITY_RESIST_GUN")

class AbilityAdditional(Enum):
	ability_stinger    = "ABILITY_ROLL_ATTACK"
	ability_negociator = "DLC_R1_KEY_ACQUIRED"
	ability_warlord    = "DLC_R2_KEY_ACQUIRED"
	ability_negator    = "DLC_R3_KEY_ACQUIRED"
