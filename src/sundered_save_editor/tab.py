from datetime import timedelta
from itertools import chain
from typing import Callable, Optional

from PySide6.QtCore import Slot
from PySide6.QtWidgets import QAbstractButton, QButtonGroup, QCheckBox, QRadioButton, QTreeWidgetItem, QWidget

from sundered_save_editor.model.abilities import Ability, AbilityAdditional, AbilityCorrupted, AbilityResist
from sundered_save_editor.model.difficulties import Difficulty
from sundered_save_editor.model.perks import PerkDefensive, PerkOffensive, PerkUtility
from sundered_save_editor.model.save import SaveGame
from sundered_save_editor.ui.tab import Ui_Tab


class Tab(QWidget):
	def __init__(self) -> None:
		super().__init__()
		self.ui = Ui_Tab()
		self.ui.setupUi(self)

		self.ui.btn_reset_difficulty.clicked.connect(self.btn_reset_difficulty_clicked)
		self.ui.btn_inc_shards.clicked.connect(self.btn_inc_shards_clicked)
		self.ui.btn_dec_shards.clicked.connect(self.btn_dec_shards_clicked)
		self.ui.btn_reset_shards.clicked.connect(self.btn_reset_shards_clicked)
		self.ui.btn_reset_totalShardsGathered.clicked.connect(self.btn_reset_totalShardsGathered_clicked)
		self.ui.btn_reset_elderFragments.clicked.connect(self.btn_reset_elderFragments_clicked)

		int_max = (1 << 31) - 1
		self.ui.spinBox_shards.setMaximum(int_max)
		self.ui.spinBox_totalShardsGathered.setMaximum(int_max)
		self.ui.spinBox_elderFragments.setMaximum(21)

		for ability in Ability:
			getattr(self.ui, ability.name).toggled.connect(self.ability_toggled)
		for ability in AbilityResist:
			getattr(self.ui, ability.name).toggled.connect(self.ability_resist_toggled)

		self.init_treewidget()

		self.data: Optional[SaveGame] = None

	def init_treewidget(self) -> None:
		assert self.ui.perkTree.columnCount() == 6
		assert self.ui.perkTree.topLevelItemCount() == 4
		assert self.ui.perkTree.topLevelItem(0).childCount() == 0
		assert self.ui.perkTree.topLevelItem(1).childCount() == len(PerkOffensive)
		assert self.ui.perkTree.topLevelItem(2).childCount() == len(PerkDefensive)
		assert self.ui.perkTree.topLevelItem(3).childCount() == len(PerkUtility)

		self.ui.perkTree.expandAll()

		self.populate_radiobuttons(self.ui.perkTree.topLevelItem(0), self.perk_all_toggled)
		for i in range(1, self.ui.perkTree.topLevelItemCount()):
			section = self.ui.perkTree.topLevelItem(i)
			for j in range(section.childCount()):
				self.populate_radiobuttons(section.child(j), self.perk_single_toggled)

		# "Eradicator" perk starts at rank 4.
		last_section = self.ui.perkTree.topLevelItem(self.ui.perkTree.topLevelItemCount() - 1)
		last_item = last_section.child(last_section.childCount() - 1)
		for k in [2, 3, 4]:
			btn: QRadioButton = self.ui.perkTree.itemWidget(last_item, k)
			btn.setEnabled(False)

		for k in range(0, self.ui.perkTree.columnCount()):
			self.ui.perkTree.resizeColumnToContents(k)

	def populate_radiobuttons(self, item: QTreeWidgetItem, slot: Callable[[QRadioButton, bool], None]) -> QButtonGroup:
		btn_group = QButtonGroup(self.ui.perkTree)
		btn_group.buttonToggled.connect(slot)
		for k in range(1, self.ui.perkTree.columnCount()):
			btn = QRadioButton()
			btn_group.addButton(btn)
			btn_group.setId(btn, k)
			self.ui.perkTree.setItemWidget(item, k, btn)
		return btn_group


	@Slot()
	def btn_reset_difficulty_clicked(self) -> None:
		assert self.data is not None
		self.ui.comboBox_difficulty.setCurrentIndex(list(Difficulty).index(Difficulty(self.data['difficulty'])))

	@Slot()
	def btn_inc_shards_clicked(self) -> None:
		self.ui.spinBox_shards.setValue(self.ui.spinBox_shards.value() + 1000)
		self.ui.spinBox_totalShardsGathered.setValue(self.ui.spinBox_totalShardsGathered.value() + 1000)

	@Slot()
	def btn_dec_shards_clicked(self) -> None:
		self.ui.spinBox_shards.setValue(self.ui.spinBox_shards.value() - 1000)
		self.ui.spinBox_totalShardsGathered.setValue(self.ui.spinBox_totalShardsGathered.value() - 1000)

	@Slot()
	def btn_reset_shards_clicked(self) -> None:
		assert self.data is not None
		self.ui.spinBox_shards.setValue(self.data['shards'])

	@Slot()
	def btn_reset_totalShardsGathered_clicked(self) -> None:
		assert self.data is not None
		self.ui.spinBox_totalShardsGathered.setValue(self.data['totalShardsGathered'])

	@Slot()
	def btn_reset_elderFragments_clicked(self) -> None:
		assert self.data is not None
		self.ui.spinBox_elderFragments.setValue(self.data['elderFragments'])

	@Slot()
	def ability_toggled(self, checked: bool) -> None:
		checkbox: QCheckBox = self.sender()
		checkbox_corrupted: QCheckBox = getattr(self.ui, checkbox.objectName() + '_corrupted')
		checkbox_corrupted.setEnabled(checked)
		checkbox_corrupted.setChecked(False)

	@Slot()
	def ability_resist_toggled(self, checked: bool) -> None:
		abilities = list(AbilityResist)
		checkbox: QCheckBox = self.sender()
		ability = AbilityResist[checkbox.objectName()]
		if checked:
			if (i := abilities.index(ability)+1) < len(abilities):
				checkbox_next: QCheckBox = getattr(self.ui, abilities[i].name)
				checkbox_next.setEnabled(True)
		else:
			for i in range(abilities.index(ability)+1, len(abilities)):
				checkbox_next: QCheckBox = getattr(self.ui, abilities[i].name) # type: ignore[no-redef]
				checkbox_next.setEnabled(False)
				checkbox_next.setChecked(False)

	@Slot()
	def perk_all_toggled(self, button: QAbstractButton, checked: bool) -> None:
		if checked:
			column = button.group().id(button)
			for i in range(1, self.ui.perkTree.topLevelItemCount()):
				section = self.ui.perkTree.topLevelItem(i)
				for j in range(section.childCount()):
					item = section.child(j)
					btn: QRadioButton = self.ui.perkTree.itemWidget(item, column)
					if btn.isEnabled():
						btn.setChecked(checked)

	@Slot()
	def perk_single_toggled(self, button: QAbstractButton, checked: bool) -> None:
		if not checked:
			column = button.group().id(button)
			item = self.ui.perkTree.topLevelItem(0)
			btn: QRadioButton = self.ui.perkTree.itemWidget(item, column)
			if btn.isChecked():
				btn.group().setExclusive(False)
				btn.setChecked(False)
				btn.group().setExclusive(True)


	def load(self, data: SaveGame) -> None:
		self.data = data

		self.ui.edit_gameplayTime.setText(str(timedelta(seconds=data['gameplayTime'])))
		self.ui.comboBox_difficulty.setCurrentIndex(list(Difficulty).index(Difficulty(data['difficulty'])))
		self.ui.spinBox_shards.setValue(data['shards'])
		self.ui.spinBox_totalShardsGathered.setValue(data['totalShardsGathered'])
		self.ui.spinBox_elderFragments.setValue(data['elderFragments'])

		flags = set(data['flags'])
		for ability in chain(Ability, AbilityCorrupted, AbilityAdditional):
			getattr(self.ui, ability.name).setChecked(ability.value in flags)
		for ability in AbilityResist:
			getattr(self.ui, ability.name).setChecked(ability.value[0] in flags)

		available_perks = set(data['perkData']['_availablePerks'])
		for i, Perk in enumerate([PerkOffensive, PerkDefensive, PerkUtility], start=1):
			for j, perk in enumerate(Perk):
				item = self.ui.perkTree.topLevelItem(i).child(j)
				column = (
					5 if f"{perk.value}_T4" in available_perks else
					4 if f"{perk.value}_T3" in available_perks else
					3 if f"{perk.value}_T2" in available_perks else
					2 if perk.value in available_perks else
					1
				)
				btn: QRadioButton = self.ui.perkTree.itemWidget(item, column)
				btn.setChecked(True)


	def save(self) -> SaveGame:
		assert self.data is not None
		self.data.update({
			'difficulty': list(Difficulty)[self.ui.comboBox_difficulty.currentIndex()].value,
			'shards': self.ui.spinBox_shards.value(),
			'totalShardsGathered': self.ui.spinBox_totalShardsGathered.value(),
			'elderFragments': self.ui.spinBox_elderFragments.value(),
		})
		available_perks = self.data['perkData']['_availablePerks']
		upgrages = self.data['upgradeSystem']['upgradeValues']
		flags = self.data['flags']

		for ability in chain(Ability, AbilityCorrupted, AbilityAdditional):
			checkbox: QCheckBox = getattr(self.ui, ability.name)
			if checkbox.isChecked() and not ability.value in flags:
				flags.append(ability.value)
			elif not checkbox.isChecked() and ability.value in flags:
				flags.remove(ability.value)

		for ability in AbilityResist:
			checkbox: QCheckBox = getattr(self.ui, ability.name) # type: ignore[no-redef]
			if checkbox.isChecked() and not ability.value[0] in flags:
				# Append only flag "RESIST_CORRUPTED_SHARD_DESTROYED_01", not "ABILITY_RESIST_ROLL",
				# because the converter cannot add upgrade values.
				flags.append(ability.value[0])
			elif not checkbox.isChecked():
				# Remove both flags "RESIST_CORRUPTED_SHARD_DESTROYED_01" and "ABILITY_RESIST_ROLL",
				# and remove upgrade value "UPGRADE_L_RESIST_1_DODGE".
				if ability.value[0] in flags:
					flags.remove(ability.value[0])
				if ability.value[3] in flags:
					flags.remove(ability.value[3])
				if ability.value[1] in upgrages:
					del upgrages[ability.value[1]]

		for i, Perk in enumerate([PerkOffensive, PerkDefensive, PerkUtility], start=1):
			for j, perk in enumerate(Perk):
				item = self.ui.perkTree.topLevelItem(i).child(j)
				if self.ui.perkTree.itemWidget(item, 5).isChecked():
					if perk == PerkUtility.perk_herald_of_the_gong:
						# "Eradicator" perk starts at rank 4.
						available_perks.append(perk.value)
					else:
						available_perks.extend([perk.value, f"{perk.value}_T2", f"{perk.value}_T3", f"{perk.value}_T4"])
				elif self.ui.perkTree.itemWidget(item, 4).isChecked():
					available_perks.extend([perk.value, f"{perk.value}_T2", f"{perk.value}_T3"])
				elif self.ui.perkTree.itemWidget(item, 3).isChecked():
					available_perks.extend([perk.value, f"{perk.value}_T2"])
				elif self.ui.perkTree.itemWidget(item, 2).isChecked():
					available_perks.append(perk.value)

		return self.data
