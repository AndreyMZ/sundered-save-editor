import faulthandler
import sys
from argparse import ArgumentParser

from PySide6.QtWidgets import QApplication

from .main_window import MainWindow
from .utils.logging import configure_logging


def main() -> None:
	parser = ArgumentParser()
	parser.add_argument("--debug", default=False, action='store_true')
	args = parser.parse_args()

	configure_logging(debug=args.debug)

	faulthandler.enable()
	app = QApplication([])
	window = MainWindow()
	# noinspection PyUnresolvedReferences
	app.aboutToQuit.connect(window.cleanup)
	window.show()
	sys.exit(app.exec())


if __name__ == '__main__':
	main()
