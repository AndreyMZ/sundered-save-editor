import logging
import sys
from collections.abc import Callable

LOG_STREAM = sys.stderr


def configure_logging(debug: bool = False) -> None:
	logging.addLevelName(logging.CRITICAL, "FATAL")
	logging.addLevelName(logging.WARNING,  "WARN")

	logging.basicConfig(
		style='{',
		format="{asctime} {levelname:5} [{name}] {message}",
		level=(logging.DEBUG if debug else logging.INFO),
		stream=LOG_STREAM,
	)


class Lazy:
	"""
	Usage example:

	>>> import subprocess
	>>> cmd = [...]
	>>> logging.debug("Command: %s", Lazy(lambda: subprocess.list2cmdline(cmd))
	"""
	def __init__(self, resolver: Callable[[], object]):
		self.resolver = resolver

	def __str__(self) -> str:
		return str(self.resolver())
