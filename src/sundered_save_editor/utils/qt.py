from PySide6.QtWidgets import QMessageBox


def ShowErrorMessageBox(text: str, details: str) -> None:
	msgBox = QMessageBox(QMessageBox.Icon.Critical, "Error", text)
	msgBox.setInformativeText(details)
	msgBox.exec()
