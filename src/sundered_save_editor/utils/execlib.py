import logging
import subprocess
from collections.abc import Sequence
from enum import IntEnum, auto
from pathlib import Path
from subprocess import CompletedProcess
from typing import Any, IO, Optional, Union

from .logging import LOG_STREAM, Lazy


class To(IntEnum):
	DEVNULL = subprocess.DEVNULL
	STDOUT  = subprocess.STDOUT
	PIPE    = subprocess.PIPE
	LOG     = auto()


def run(command: Sequence[Union[str, Path]], *,
        stdin:  Union[None, IO[Any], Path] = None,
        stdout: Union[None, IO[Any], Path, To] = To.LOG,
        stderr: Union[None, IO[Any], Path, To] = To.LOG,
        log_level: int = logging.DEBUG,
        **kwargs: Any) -> CompletedProcess[bytes]:

	logging.log(log_level, "COMMAND: %s", Lazy(lambda: subprocess.list2cmdline(command)))

	fin:  Optional[IO[bytes]] = None
	fout: Optional[IO[bytes]] = None
	ferr: Optional[IO[bytes]] = None
	try:
		_stdin:  Union[None, int, IO[Any]]
		_stdout: Union[None, int, IO[Any]]
		_stderr: Union[None, int, IO[Any]]

		if isinstance(stdin, Path):
			_stdin = fin = stdin.open('rb')
		else:
			_stdin = stdin

		if isinstance(stdout, Path):
			_stdout = fout = stdout.open('wb')
		elif isinstance(stdout, To):
			_stdout = LOG_STREAM if stdout is To.LOG else stdout.value
		else:
			_stdout = stdout

		if isinstance(stderr, Path):
			_stderr = ferr = stderr.open('wb')
		elif isinstance(stderr, To):
			_stderr = LOG_STREAM if stderr is To.LOG else stderr.value
		else:
			_stderr = stderr

		if (stdout is To.LOG) and ((stderr is To.LOG) or (stderr is To.STDOUT)):
			logging.log(log_level, "STDOUT & STDERR:")
		elif stderr is To.LOG:
			logging.log(log_level, "STDERR:")
		elif stdout is To.LOG:
			logging.log(log_level, "STDOUT:")

		p = subprocess.run(command, stdin=_stdin, stdout=_stdout, stderr=_stderr, **kwargs)
	finally:
		if fin is not None:
			fin.close()
		if fout is not None:
			fout.close()
		if ferr is not None:
			ferr.close()

	logging.log(log_level, "EXIT STATUS: %s", p.returncode)
	if p.stdout is not None:
		assert isinstance(p.stdout, bytes)
		if len(p.stdout) > 0:
			logging.log(log_level, "STDOUT:\n%s", Lazy(lambda: p.stdout.decode('UTF-8', errors='replace')))
	if p.stderr is not None:
		assert isinstance(p.stderr, bytes)
		if len(p.stderr) > 0:
			logging.log(log_level, "STDERR:\n%s", Lazy(lambda: p.stderr.decode('UTF-8', errors='replace')))

	return p
