import argparse
import re
import subprocess
from collections import defaultdict
from pathlib import Path
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import Element


def main() -> None:
	parser = argparse.ArgumentParser()
	parser.add_argument("files_pattern", type=str)
	parser.add_argument("output_dir", type=Path)
	args = parser.parse_args()

	for filepath in Path.cwd().glob(args.files_pattern):
		print(f"Processing {filepath}")
		beautify(filepath)
		translate(filepath, args.output_dir)


def beautify(filepath: Path) -> None:
	tree = ET.parse(filepath)
	visit_element(tree.getroot())
	ET.indent(tree, space="  ")
	# filepath.rename(filepath.with_suffix(f"{filepath.suffix}.bak"))
	tree.write(filepath)


def translate(filepath: Path, output_dir: Path) -> None:
	output_path = output_dir.joinpath(Path(pascal_to_snake(filepath.stem)).with_suffix(".py"))
	subprocess.check_call(["pyside6-uic", filepath, "--output", output_path])


def visit_element(el: Element) -> None:
	if el.tag == "layout" and el.get("class") in ["QGridLayout", "QFormLayout"]:
		sort_grid_items(el)
	if el.tag in ["layout", "widget"]:
		fix_name(el)

	for child in el:
		visit_element(child)


def sort_grid_items(el: Element) -> None:
	items = el.findall("item")
	for item in items:
		el.remove(item)
	items.sort(key=(lambda it: (int(it.attrib["row"]), int(it.attrib["column"]))))
	for item in items:
		el.append(item)


default_names: dict[str, str] = {
	"QVBoxLayout": "verticalLayout",
	"QHBoxLayout": "horizontalLayout",
}
names_map: dict[str, str] = {}
names_counter: dict[str, int] = defaultdict(lambda: 0)

def fix_name(el: Element) -> None:
	clss = el.get("class")
	name = el.get("name")
	assert clss is not None
	assert name is not None
	default_name = default_names.get(clss) or clss[1].lower() + clss[2:]
	if re.fullmatch(rf"{re.escape(default_name)}(_\d+)?", name):
		new_name = names_map.get(name)
		if new_name is None:
			names_counter[default_name] += 1
			new_name = f"{default_name}_{names_counter[default_name]}"
			names_map[name] = new_name
		el.set("name", new_name)


def pascal_to_snake(string: str) -> str:
	return re.sub(r'(.)([A-Z])', r'\1_\2', string).lower()


if __name__ == '__main__':
	main()
